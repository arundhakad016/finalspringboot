FROM openjdk:11
VOLUME /tmp
ADD target/app.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]