package com.insurance.controller.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import com.insurance.controller.UserController;
import com.insurance.exception.MyApplicationException;
import com.insurance.model.Claim;
import com.insurance.model.Nominee;
import com.insurance.model.Policy;
import com.insurance.model.Role;
import com.insurance.model.User;
import com.insurance.model.UserDependents;
import com.insurance.model.UsersPolicy;
import com.insurance.model.enums.Category;
import com.insurance.model.enums.Gender;
import com.insurance.model.enums.Status;
import com.insurance.service.AdminService;
import com.insurance.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
class UserControllerTest{

	@MockBean
	public UserService userService;

	@MockBean
	public AdminService adminService;

	@Autowired
	UserController userController;

	@Test
    void registerUserTest() {

		User user = new User(1, "virat", "virat@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road",
				new java.util.Date(), false, null, new Role("ROLE_USER"));

		when(adminService.save(user)).thenReturn(user);
		assertEquals(user, userController.registerUser(user));
	}

	@Test
	void updateUserProfileTest() {
		// user object
		User user = new User(1, "virat", "virat@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road",
				new java.util.Date(), false, null, new Role("ROLE_USER"));

		when(userService.updateUserProfile(user)).thenReturn(user);
		assertEquals(user, userController.updateUserProfile(user));

	}

	@Test
	void addDependentTest() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		UserDependents userDependents1 = new UserDependents(1, "dep1", date, false, null, "father", user);
		UserDependents userDependents2 = new UserDependents(2, "dep2", date, false, null, "father", user);

		List<UserDependents> l = new ArrayList<UserDependents>();
		l.add(userDependents1);
		l.add(userDependents2);

		when(userService.addDependents(l)).thenReturn(l);
		assertEquals(l, userController.addDependent(l));
	}

	@Test
	void updateDependentsTest() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		UserDependents userDependents1 = new UserDependents(1, "dep1", date, false, null, "father", user);

		when(userService.updateDependent(userDependents1)).thenReturn(userDependents1);

		assertEquals(userDependents1, userController.updateDependent(userDependents1));
	}

	@Test
	void getAllDependentsTest() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		UserDependents userDependents1 = new UserDependents(1, "dep1", date, false, null, "father", user);
		UserDependents userDependents2 = new UserDependents(2, "dep2", date, false, null, "father", user);

		List<UserDependents> l = new ArrayList<UserDependents>();
		l.add(userDependents1);
		l.add(userDependents2);

		when(userService.getAllDependents()).thenReturn(l);
		assertEquals(l, userController.getAllDependents());
	}

	@Test
	void getPoliciesByCategoryTest() {
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);
		List<Policy> l = new ArrayList<Policy>();
		l.add(policy);

		when(userService.getPoliciesByCategory("Life")).thenReturn(l);

		assertEquals(l, userController.getPoliciesByCategory("Life"));
	}

	@Test
	void getDependentsByPolicyMaxAgeTest() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		// list of userDependents
		UserDependents userDependents1 = new UserDependents(1, "dep1", date, false, null, "father", user);
		UserDependents userDependents2 = new UserDependents(2, "dep2", date, false, null, "father", user);

		List<UserDependents> l = new ArrayList<UserDependents>();
		l.add(userDependents1);
		l.add(userDependents2);

		when(userService.getDependentsByPolicyMaxAge(42)).thenReturn(l);

		assertEquals(l, userController.getDependentsByPolicyMaxAge(42));

	}

	@Test
	void getUserPolicies() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));
		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);

		List<UsersPolicy> list = new ArrayList<UsersPolicy>();
		list.add(usersPolicy);

		when(userService.getAllUserPolicies()).thenReturn(list);

		assertEquals(list, userController.getUserPolicies());
	}

	@Test
	void purchasePolicyTest() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);

		when(userService.purchasePolicy(usersPolicy)).thenReturn(usersPolicy);

		assertEquals(usersPolicy, userController.purchasePolicy(usersPolicy));
	}

	@Test
	void updateUsersPolicyTest() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);

		when(userService.updateUsersPolicy(usersPolicy)).thenReturn(usersPolicy);

		assertEquals(usersPolicy, userController.updateUsersPolicy(usersPolicy));
	}

	@Test
	void getUserPoliciesByStatusTest() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));
		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);
		List<UsersPolicy> list = new ArrayList<UsersPolicy>();
		list.add(usersPolicy);

		when(userService.getUserPoliciesByStatus("Approved")).thenReturn(list);

		assertEquals(list, userController.getUserPoliciesByStatus("Approved"));

	}

	@Test
	void addNomineeTest() {
		Nominee nominee = new Nominee();
		nominee.setId(1);
		nominee.setName("suyash");
		nominee.setPhoneNumber(1234567899);
		when(userService.addNominee(nominee)).thenReturn(nominee);

		assertEquals(nominee, userController.addNominee(nominee));
		assertEquals(nominee.getId(),userController.addNominee(nominee).getId());
		assertEquals(nominee.getName(), userController.addNominee(nominee).getName());
		assertEquals(nominee.getPhoneNumber(), userController.addNominee(nominee).getPhoneNumber());
	}

	@Test
	void addClaimTest() {
		Date date = new java.util.Date();
		User user = new User(1, "virat", "virat@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road",
				date, false, null, new Role("ROLE_USER"));

		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);

		Claim claim = new Claim(1, "claim1", date, 0, 5000, usersPolicy);

		when(userService.addClaim(claim)).thenReturn(claim);

		// String JsonRequest = objectMapper.writeValueAsString(claim);

		// System.out.println(JsonRequest);
//        MvcResult result = mockMvc
//                .perform(post("/user/addClaim")
//                        .content(JsonRequest).accept(MediaType.APPLICATION_JSON_VALUE))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType("application/json;charset=UTF-8"))
//                .andReturn();

		// String resultContext = result.getResponse().getContentAsString();

		// Claim cl = objectMapper.readValue(resultContext, Claim.class);

		assertEquals(claim, userController.addClaim(claim));
		// assertTrue(cl.getClaimedPerson().equals("claim1"));

	}

	@Test
	void getUserClaimsByUserPolicyTest() throws MyApplicationException {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));
		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);

		Claim claim = new Claim(1, "claim1", date, 0, 5000, usersPolicy);

		List<Claim> list = new ArrayList<Claim>();
		list.add(claim);

		when(userService.getUserClaimsByUserPolicy(1)).thenReturn(list);

		assertEquals(list, userController.getUserClaimsByUserPolicy(1));

	}

}
