package com.insurance.controller.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.security.Principal;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import com.insurance.config.CustomUserDetails;
import com.insurance.controller.TokenController;
import com.insurance.model.AuthenticationRequest;
import com.insurance.model.AuthenticationResponse;
import com.insurance.model.Role;
import com.insurance.model.User;
import com.insurance.model.enums.Gender;
import com.insurance.repository.UserRepository;
import com.insurance.util.JwtUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
class TokenControllerTest{

	@MockBean
	UserDetailsService userDetailsService;

	
	private CustomUserDetails customUserDetails;

	@MockBean
	AuthenticationRequest authenticationRequest;
	
	
	AuthenticationResponse authenticationResponse;
	
	@MockBean
	JwtUtil jwtUtil;
	
	
	@MockBean
	Principal principal;
	

	@MockBean
	UserRepository userRepository;

	@Autowired
	private TokenController tokenController;

	@Test
	void testCreateAuthenticationToken() {
		authenticationRequest.setUsername("yuvraj@gmail.com");
		Date date = new java.util.Date();
		User user = new User();
		user.setId(1);
		user.setName("yuvraj");
		user.setEmail("yuvraj@gmail.com");
		user.setPassword("123");
		user.setPhoneNumber("8989898989");
		user.setGender(Gender.valueOf("Male"));
		user.setAddress("Mg Road");
		user.setDob(date);
		user.setMedicalCondition(false);
		user.setMedicalConditionDesc(null);
		Role role = new Role();
		role.setId(3);
		role.setName("ROLE_USER");
		user.setRoles(role);
		customUserDetails=new CustomUserDetails(user);
		when(userDetailsService.loadUserByUsername(authenticationRequest.getUsername())).thenReturn(customUserDetails);
		when(jwtUtil.generateToken(customUserDetails)).thenReturn("yuvraj");
		authenticationResponse=new AuthenticationResponse("yuvraj");
		assertEquals(customUserDetails,userDetailsService.loadUserByUsername(authenticationRequest.getUsername()));
		assertEquals("yuvraj", jwtUtil.generateToken(customUserDetails));
		assertEquals("yuvraj", authenticationResponse.getJwt());
		
	}
	
	@Test
	void testGetCurrentUse()
	{
		Date date = new java.util.Date();
		User user = new User();
		user.setId(1);
		user.setName("yuvraj");
		user.setEmail("yuvraj@gmail.com");
		user.setPassword("123");
		user.setPhoneNumber("8989898989");
		user.setGender(Gender.valueOf("Male"));
		user.setAddress("Mg Road");
		user.setDob(date);
		user.setMedicalCondition(false);
		user.setMedicalConditionDesc(null);
		Role role = new Role();
		role.setId(3);
		role.setName("ROLE_USER");
		user.setRoles(role);
		String email="yuvraj@gmail.com";
		when(principal.getName()).thenReturn(email);
		when(userRepository.findByEmail(email)).thenReturn(user);
		assertEquals(user,tokenController.getCurrentUser(principal));
		assertEquals(1, user.getId());
		assertEquals("yuvraj", user.getName());
		assertEquals("yuvraj@gmail.com", user.getEmail());
		assertEquals("123", user.getPassword());
		assertEquals("8989898989", user.getPhoneNumber());
		assertEquals(Gender.valueOf("Male"), user.getGender());
		assertEquals("Mg Road", user.getAddress());
		assertEquals(date, user.getDob());
		assertEquals(false, user.isMedicalCondition());
		assertEquals(null, user.getMedicalConditionDesc());
	}

}
