package com.insurance.controller.test;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import com.insurance.controller.AdminController;
import com.insurance.exception.MyApplicationException;
import com.insurance.model.Nominee;
import com.insurance.model.Policy;
import com.insurance.model.Role;
import com.insurance.model.User;
import com.insurance.model.UserDependents;
import com.insurance.model.UsersPolicy;
import com.insurance.model.dto.SoldPolicyStatus;
import com.insurance.model.enums.Category;
import com.insurance.model.enums.Gender;
import com.insurance.model.enums.Status;
import com.insurance.service.AdminService;
import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser
class AdminControllerTest{

	@MockBean
	private AdminService adminService;
	
	@MockBean
	private Role role;
	
	@Autowired
	private AdminController adminController;
	
	@Test
	void getAllUsers() {
		java.util.Date date = new java.util.Date();
		when(adminService.getAllUsers()).thenReturn(Stream
				.of(new User(1,"Yuvraj", "yuvraj@gmail.com", "123", "8989898989", Gender.valueOf("Male"), "Shajapur",
						date, false, null,new Role("ROLE_UNDERWRITER")),new User(2,"Raj", "raj@gmail.com", "123", "8989898989", Gender.valueOf("Male"), "Shajapur",
								date, false, null,new Role("ROLE_UNDERWRITER")))
				.collect(Collectors.toList()));
		assertEquals(2, adminController.getAllUsers().size());
	}
	
	@Test
	void addPolicyTest() {
		Policy policy = new Policy();
		policy.setId(1);
		policy.setName("Jeevan Yojna");
		policy.setCategory(Category.valueOf("Life"));
		policy.setDescription("Life Policy");
		policy.setTenure(1);
		policy.setCoverage(100000);
		policy.setMarkedPremium(1000);
		policy.setMaxAge(45);
		when(adminService.addPolicy(policy)).thenReturn(policy);
		assertEquals(policy, adminController.addPolicy(policy));
		assertEquals(1, adminController.addPolicy(policy).getId());
        assertEquals("Jeevan Yojna", adminController.addPolicy(policy).getName());
        assertEquals(Category.valueOf("Life"), adminController.addPolicy(policy).getCategory());
        assertEquals("Life Policy", adminController.addPolicy(policy).getDescription());
        assertEquals(1, adminController.addPolicy(policy).getTenure());
        assertEquals(100000, adminController.addPolicy(policy).getCoverage());
        assertEquals(45, adminController.addPolicy(policy).getMaxAge());
        assertEquals(1000, adminController.addPolicy(policy).getMarkedPremium());		
		
	}

	@Test
	void getAllPoliciesTest() {
		when(adminService.getAllPolicies()).thenReturn(Stream
				.of(new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45),
						new Policy(2, "Lifeline 4 U", "Life Policy 2", Category.valueOf("Life"), 1000, 100000, 1, 45))
				.collect(Collectors.toList()));
		assertEquals(2, adminController.getAllPolicies().size());
	}

	@Test
	void updatePolicyTest() {
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);
		when(adminService.updatePolicy(policy)).thenReturn(policy);
		assertEquals(policy, adminController.updatePolicy(policy));
	}

	@Test
	void deletePolicyByIdTest() throws MyApplicationException {
		int id = 1;
		adminController.deletePolicy(id);
		verify(adminService, times(1)).deletePolicyById(id);
	}

	@Test
	void saveUnderwriterTest() {

		java.util.Date date = new java.util.Date();
		User user = new User(1, "Yuvraj", "yuvraj@gmail.com", "123", "8989898989", Gender.valueOf("Male"), "Shajapur",
				date, false, null, new Role("ROLE_UNDERWRITER"));
		when(adminService.save(user)).thenReturn(user);
		assertEquals(user, adminController.addUnderwriter(user));
	}

	@Test
	void getAllUnderwritersTest() {
		java.util.Date date = new java.util.Date();
		when(adminService.getAllUnderwriters(null)).thenReturn(Stream
				.of(new User(1,"Yuvraj", "yuvraj@gmail.com", "123", "8989898989", Gender.valueOf("Male"), "Shajapur",
						date, false, null,new Role("ROLE_UNDERWRITER")),new User(2,"Raj", "raj@gmail.com", "123", "8989898989", Gender.valueOf("Male"), "Shajapur",
								date, false, null,new Role("ROLE_UNDERWRITER")))
				.collect(Collectors.toList()));
		assertEquals(0, adminController.getAllUnderwriters().size());
	}
	
	@Test
	void updateUnderwriterTest() {

		java.util.Date date = new java.util.Date();
		User user = new User(1, "Yuvraj", "yuvraj@gmail.com", "123", "8989898989", Gender.valueOf("Male"), "Shajapur",
				date, false, null, new Role("ROLE_UNDERWRITER"));
		when(adminService.updateUnderwriter(user)).thenReturn(user);
		assertEquals(user, adminController.updateUnderwriter(user));
	}

	@Test
	void deleteUnderwriterByIdTest() throws MyApplicationException {
		int id = 1;
		adminController.deleteUnderwriter(id);
		verify(adminService, times(1)).deleteUnderwriterById(id);
	}
	
	@Test
	void policyWithCellStatusTest()
	{
		Policy policy1 = new Policy(2, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);
        SoldPolicyStatus soldPolicyStatus=new SoldPolicyStatus();
        soldPolicyStatus.setPolicy(policy1);
        soldPolicyStatus.setSold(true);
        List<SoldPolicyStatus> userPolicies=new ArrayList<>();
        userPolicies.add(soldPolicyStatus);
        List<Policy> policies=new ArrayList<>();       
        policies.add(policy1);
        when(adminService.policyWithSellStatus()).thenReturn(userPolicies);
        assertEquals(1, adminController.policyWithSellStatus().size());
		
		
	}
	@Test
	void getPurchasedPolicyTest() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));
		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);
		List<UsersPolicy> list = new ArrayList<UsersPolicy>();
		list.add(usersPolicy);
		when(adminService.getPurchasedPolicy()).thenReturn(list);
		assertEquals(1, adminController.getPurchasedPolicy().size());
	}
	
	
	
}
