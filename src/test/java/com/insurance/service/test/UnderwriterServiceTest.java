package com.insurance.service.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.insurance.model.Nominee;
import com.insurance.model.Policy;
import com.insurance.model.Role;
import com.insurance.model.UserDependents;
import com.insurance.model.UsersPolicy;
import com.insurance.model.User;
import com.insurance.model.enums.Category;
import com.insurance.model.enums.Gender;
import com.insurance.model.enums.Status;
import com.insurance.repository.UsersPolicyRepository;
import com.insurance.service.UnderwriterService;

@RunWith(SpringRunner.class)
@SpringBootTest
class UnderwriterServiceTest{

	@MockBean
	private UsersPolicyRepository usersPolicyRepository;

	@Autowired
	private UnderwriterService underwriterService;

	@Test
	void getUserPoliciesByStatus() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_UNDERWRITER"));

		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Pending"), 5000, date, date, false,
				0, l, nominee);

		List<UsersPolicy> list = new ArrayList<UsersPolicy>();
		list.add(usersPolicy);
		when(usersPolicyRepository.findByStatus(Status.valueOf("Pending"))).thenReturn(list);
		assertEquals(1, underwriterService.getUserPoliciesByStatus("Pending").size());
	}

	@Test
	void updateUsersPolicyTest(){
		Date date=new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);

		when(usersPolicyRepository.save(usersPolicy)).thenReturn(usersPolicy);
		assertEquals(usersPolicy, underwriterService.updateUsersPolicy(usersPolicy));
	}

}
