package com.insurance.service.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.insurance.model.Nominee;
import com.insurance.model.Policy;
import com.insurance.model.Role;
import com.insurance.model.User;
import com.insurance.model.UserDependents;
import com.insurance.model.UsersPolicy;
import com.insurance.model.enums.Category;
import com.insurance.model.enums.Gender;
import com.insurance.model.enums.Status;
import com.insurance.repository.UsersPolicyRepository;
import com.insurance.service.SchedulerService;

@RunWith(SpringRunner.class)
@SpringBootTest
class SchedulerServiceTest{

	@MockBean
    private UsersPolicyRepository usersPolicyRepository;

 

    @Autowired
    private SchedulerService schedulerService;

 

    @Test
    void checkingExpiryOfUsersPolicyTest() {
        // user object
        User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", null,
                false, null, new Role("ROLE_USER"));
        // policy object
        Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

 

        // nominee object
        Nominee nominee = new Nominee(1, "nominee1", 1234567899);

 

        Collection<UserDependents> l = new ArrayList<UserDependents>();

 

        UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, null, null, false,
                0, l, nominee);

 

        List<UsersPolicy> list = new ArrayList<UsersPolicy>();
        list.add(usersPolicy);
        when(usersPolicyRepository.findAll()).thenReturn(list);

 

        assertThat(schedulerService.getInvocationCount1()).isEqualTo(1);

 

    }

 

    @Test
    void removeRejectedPoliciesTest() {
        // user object
        User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", null,
                false, null, new Role("ROLE_USER"));
        // policy object
        Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

 

        // nominee object
        Nominee nominee = new Nominee(1, "nominee1", 1234567899);

 

        Collection<UserDependents> l = new ArrayList<UserDependents>();

 

        UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, null, null, false,
                0, l, nominee);

 

        List<UsersPolicy> list = new ArrayList<UsersPolicy>();
        list.add(usersPolicy);
        when(usersPolicyRepository.findAll()).thenReturn(list);

 

        assertThat(schedulerService.getInvocationCount2()).isEqualTo(1);
    }
 
}
