package com.insurance.service.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.insurance.config.UserDetailsServiceImpl;
import com.insurance.exception.MyApplicationException;
import com.insurance.model.Claim;
import com.insurance.model.Nominee;
import com.insurance.model.Policy;
import com.insurance.model.Role;
import com.insurance.model.User;
import com.insurance.model.UserDependents;
import com.insurance.model.UsersPolicy;
import com.insurance.model.enums.Category;
import com.insurance.model.enums.Gender;
import com.insurance.model.enums.Status;
import com.insurance.repository.ClaimRepository;
import com.insurance.repository.DependentsRepository;
import com.insurance.repository.NomineeRepository;
import com.insurance.repository.PolicyRepository;
import com.insurance.repository.UserRepository;
import com.insurance.repository.UsersPolicyRepository;
import com.insurance.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
class UserServiceTest{
	@MockBean
	private UserRepository userRepository;

	@MockBean
	private NomineeRepository nomineeRepository;

	@MockBean
	private UsersPolicyRepository usersPolicyRepository;

	@MockBean
	private ClaimRepository claimRepository;

	@MockBean
	private PolicyRepository policyRepository;

	@MockBean
	private DependentsRepository dependentsRepository;

	@MockBean
	private UserDetailsServiceImpl userDetailsServiceImpl;

	@Autowired
	private UserService userService;

	@Test
	void updateUserProfile() {
		Date date = new java.util.Date();

		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));
		when(userRepository.save(user)).thenReturn(user);
		assertEquals(user, userService.updateUserProfile(user));
	}

	@Test
	void addNomineeTest() {
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);
		when(nomineeRepository.save(nominee)).thenReturn(nominee);
		assertEquals(nominee, userService.addNominee(nominee));
	}

	@Test
	void addUsersPolicyTest() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();
		UsersPolicy usersPolicy = new UsersPolicy();
		usersPolicy.setId(1);
		usersPolicy.setUser(user);
		usersPolicy.setPolicy(policy);
		usersPolicy.setStatus(Status.valueOf("Approved"));
		usersPolicy.setClaimedAmount(5000);
		usersPolicy.setPurchasedDate(date);
		usersPolicy.setExpireDate(date);
		usersPolicy.setHasDependent(false);
		usersPolicy.setNominee(nominee);
		usersPolicy.setSoldPremium(0);
		usersPolicy.setUserDependents(l);

		when(usersPolicyRepository.save(usersPolicy)).thenReturn(usersPolicy);
		assertEquals(usersPolicy, userService.purchasePolicy(usersPolicy));
		assertEquals(usersPolicy.getId(), userService.purchasePolicy(usersPolicy).getId());
		assertEquals(usersPolicy.getNominee(), userService.purchasePolicy(usersPolicy).getNominee());
		assertEquals(usersPolicy.getClaimedAmount(), userService.purchasePolicy(usersPolicy).getClaimedAmount());
		assertEquals(usersPolicy.getExpireDate(), userService.purchasePolicy(usersPolicy).getExpireDate());
		assertEquals(usersPolicy.getPurchasedDate(), userService.purchasePolicy(usersPolicy).getPurchasedDate());
		assertEquals(usersPolicy.getPolicy(), userService.purchasePolicy(usersPolicy).getPolicy());
		assertEquals(usersPolicy.getSoldPremium(), userService.purchasePolicy(usersPolicy).getSoldPremium());
		assertEquals(usersPolicy.getStatus(), userService.purchasePolicy(usersPolicy).getStatus());
		assertEquals(usersPolicy.getUser(), userService.purchasePolicy(usersPolicy).getUser());
		assertEquals(usersPolicy.getUserDependents(), userService.purchasePolicy(usersPolicy).getUserDependents());

	}

	@Test
	void addClaimTest(){
		Date date=new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);

		Claim claim = new Claim();
		claim.setId(1);
		claim.setClaimedPerson("claim1");
		claim.setClaimDate(date);
		claim.setClaimAmount(0);
		claim.setRemainingAmount(5000);
		claim.setUsersPolicy(usersPolicy);
		when(claimRepository.save(claim)).thenReturn(claim);
		assertEquals(claim, userService.addClaim(claim));
		assertEquals(claim.getId(), userService.addClaim(claim).getId());
		assertEquals(claim.getClaimedPerson(), userService.addClaim(claim).getClaimedPerson());
		assertEquals(claim.getClaimDate(), userService.addClaim(claim).getClaimDate());
		assertEquals(claim.getRemainingAmount(), userService.addClaim(claim).getRemainingAmount());
		assertEquals(claim.getUsersPolicy(), userService.addClaim(claim).getUsersPolicy());
		assertEquals(claim.getClaimAmount(), userService.addClaim(claim).getClaimAmount());

	}

	@Test
	void addDependentsTest() {
		Date date=new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		UserDependents userDependents = new UserDependents();
		userDependents.setId(1);
		userDependents.setName("dep1");
		userDependents.setDob(date);
		userDependents.setMedicalCondition(false);
		userDependents.setMedicalConditionDesc(null);
		userDependents.setRelation("father");
		userDependents.setUser(user);
		List<UserDependents> l = new ArrayList<UserDependents>();
		l.add(userDependents);
		when(dependentsRepository.save(userDependents)).thenReturn(userDependents);
		assertEquals(l, userService.addDependents(l));
		assertEquals(userDependents.getId(), userService.addDependents(l).get(0).getId());
		assertEquals(userDependents.getName(), userService.addDependents(l).get(0).getName());
		assertEquals(userDependents.getDob(), userService.addDependents(l).get(0).getDob());
		assertEquals(userDependents.getMedicalConditionDesc(),userService.addDependents(l).get(0).getMedicalConditionDesc());
		assertEquals(userDependents.getRelation(), userService.addDependents(l).get(0).getRelation());
		assertEquals(userDependents.getUser(), userService.addDependents(l).get(0).getUser());

	}

	@Test
	void updateDependentTest() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		UserDependents userDependents = new UserDependents(1, "dep1", date, false, null, "father", user);
		when(dependentsRepository.save(userDependents)).thenReturn(userDependents);
		assertEquals(userDependents, userService.updateDependent(userDependents));

	}

	@Test
	void getAllDependents(){
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		// list of userDependents
		UserDependents userDependents1 = new UserDependents(1, "dep1", date, false, null, "father", user);
		UserDependents userDependents2 = new UserDependents(2, "dep2", date, false, null, "father", user);

		List<UserDependents> l = new ArrayList<UserDependents>();
		l.add(userDependents1);
		l.add(userDependents2);

		String email = "abc@gmail.com";
		when(userDetailsServiceImpl.currentName()).thenReturn(email);
		when(userRepository.findByEmail(email)).thenReturn(user);
		when(dependentsRepository.findByUser(user)).thenReturn(l);
		assertEquals(2, l.size());
	}

	@Test
	void getDependentsByPolicyMaxAgeTest() {
		Date date=new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		// list of userDependents
		UserDependents userDependents1 = new UserDependents(1, "dep1", date, false, null, "father", user);
		UserDependents userDependents2 = new UserDependents(2, "dep2", date, false, null, "father", user);

		List<UserDependents> l = new ArrayList<UserDependents>();
		l.add(userDependents1);
		l.add(userDependents2);

		String email = "abc@gmail.com";
		when(userDetailsServiceImpl.currentName()).thenReturn(email);
		when(userRepository.findByEmail(email)).thenReturn(user);
		when(dependentsRepository.findByUser(user)).thenReturn(l);

		assertEquals(2, l.size());
	}

	@Test
	void getPoliciesByCategoryTest() {
		Date date=new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));

		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);
		List<Policy> l = new ArrayList<Policy>();
		l.add(policy);

		String email = "abc@gmail.com";
		when(userDetailsServiceImpl.currentName()).thenReturn(email);
		when(userRepository.findByEmail(email)).thenReturn(user);
		when(policyRepository.findByCategory(Category.valueOf("Life"))).thenReturn(l);

		assertEquals(1, l.size());
	}

	@Test
	void getAllUserPoliciesTest() {
		Date date=new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));
		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);

		List<UsersPolicy> list = new ArrayList<UsersPolicy>();
		list.add(usersPolicy);

		String email = "abc@gmail.com";
		when(userDetailsServiceImpl.currentName()).thenReturn(email);
		when(userRepository.findByEmail(email)).thenReturn(user);
		when(usersPolicyRepository.findByUser(user)).thenReturn(list);

		assertEquals(0, l.size());
	}

	@Test
	void updateUsersPolicyTest() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));
		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);

		when(usersPolicyRepository.save(usersPolicy)).thenReturn(usersPolicy);
		assertEquals(usersPolicy, userService.updateUsersPolicy(usersPolicy));
	}

	@Test
	void getUserPoliciesByStatusTest() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));
		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);
		List<UsersPolicy> list = new ArrayList<UsersPolicy>();
		list.add(usersPolicy);

		String email = "abc@gmail.com";
		when(userDetailsServiceImpl.currentName()).thenReturn(email);
		when(userRepository.findByEmail(email)).thenReturn(user);
		when(usersPolicyRepository.findByStatusAndUser(Status.valueOf("Approved"), user)).thenReturn(list);

		assertEquals(1, list.size());
	}

	@Test
	void getUserClaimsByUserPolicyTest() throws MyApplicationException {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));
		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);

		Claim claim = new Claim(1, "claim1", date, 0, 5000, usersPolicy);

		List<Claim> list = new ArrayList<Claim>();
		list.add(claim);
		when(usersPolicyRepository.findById(1)).thenReturn(Optional.ofNullable(usersPolicy));
		when(claimRepository.findByUsersPolicy(usersPolicy)).thenReturn(list);

		assertEquals(1, userService.getUserClaimsByUserPolicy(1).size());

	}

}
