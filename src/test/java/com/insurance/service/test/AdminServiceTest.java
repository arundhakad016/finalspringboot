package com.insurance.service.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.insurance.exception.MyApplicationException;
import com.insurance.model.Nominee;
import com.insurance.model.Policy;
import com.insurance.model.Role;
import com.insurance.model.User;
import com.insurance.model.UserDependents;
import com.insurance.model.UsersPolicy;
import com.insurance.model.enums.Category;
import com.insurance.model.enums.Gender;
import com.insurance.model.enums.Status;
import com.insurance.repository.PolicyRepository;
import com.insurance.repository.UserRepository;
import com.insurance.repository.UsersPolicyRepository;
import com.insurance.service.AdminService;

@RunWith(SpringRunner.class)
@SpringBootTest
class AdminServiceTest{
	@Autowired
	private AdminService adminService;

	@MockBean
	private PolicyRepository policyRepository;

	@MockBean
	private UserRepository userRepository;
	
	@MockBean
	private UsersPolicyRepository usersPolicyRepository;

	
	@Test
	void getAllUsers() {
		java.util.Date date = new java.util.Date();
		when(userRepository.findAll()).thenReturn(Stream
				.of(new User(1,"Yuvraj", "yuvraj@gmail.com", "123", "8989898989", Gender.valueOf("Male"), "Shajapur",
						date, false, null,new Role("ROLE_UNDERWRITER")),new User(2,"Raj", "raj@gmail.com", "123", "8989898989", Gender.valueOf("Male"), "Shajapur",
								date, false, null,new Role("ROLE_UNDERWRITER")))
				.collect(Collectors.toList()));
		assertEquals(2, adminService.getAllUsers().size());
	}
	
	
	@Test
	void addPolicyTest() {
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);
		when(policyRepository.save(policy)).thenReturn(policy);
		assertEquals(policy, adminService.addPolicy(policy));
	}

	@Test
	void getAllPoliciesTest() {
		when(policyRepository.findAll()).thenReturn(Stream
				.of(new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45),
						new Policy(2, "Lifeline 4 U", "Life Policy 2", Category.valueOf("Life"), 1000, 100000, 1, 45))
				.collect(Collectors.toList()));
		assertEquals(2, adminService.getAllPolicies().size());
	}

	@Test
	void updatePolicyTest() {
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);
		when(policyRepository.save(policy)).thenReturn(policy);
		assertEquals(policy, adminService.updatePolicy(policy));
	}

	@Test
	void deletePolicyByIdTest() throws MyApplicationException {
		int id = 1;
		adminService.deletePolicyById(id);
		verify(policyRepository, times(1)).deleteById(id);
	}

	@Test
	void saveUnderwriterTest() {

		java.util.Date date = new java.util.Date();
		User user = new User(1, "Yuvraj", "yuvraj@gmail.com", "123", "8989898989", Gender.valueOf("Male"), "Shajapur",
				date, false, null, new Role("ROLE_UNDERWRITER"));
		when(userRepository.save(user)).thenReturn(user);
		assertEquals(user, adminService.save(user));
	}

	@Test
	void getAllUnderwritersTest() {
		Role role=new Role("ROLE_UNDERWRITER");
		java.util.Date date = new java.util.Date();
		when(userRepository.findByRole(role)).thenReturn(Stream
				.of(new User(1,"Yuvraj", "yuvraj@gmail.com", "123", "8989898989", Gender.valueOf("Male"), "Shajapur",
						date, false, null,new Role("ROLE_UNDERWRITER")),new User(2,"Raj", "raj@gmail.com", "123", "8989898989", Gender.valueOf("Male"), "Shajapur",
								date, false, null,new Role("ROLE_UNDERWRITER")))
				.collect(Collectors.toList()));
		assertEquals(2, adminService.getAllUnderwriters(role).size());
	}
	
	@Test
	void updateUnderwriterTest() {

		java.util.Date date = new java.util.Date();
		User user = new User(1, "Yuvraj", "yuvraj@gmail.com", "123", "8989898989", Gender.valueOf("Male"), "Shajapur",
				date, false, null, new Role("ROLE_UNDERWRITER"));
		when(userRepository.save(user)).thenReturn(user);
		assertEquals(user, adminService.updateUnderwriter(user));
	}

	@Test
	void deleteUnderwriterByIdTest() throws MyApplicationException {
		int id = 1;
		adminService.deleteUnderwriterById(id);
		verify(userRepository, times(1)).deleteById(id);
	}
	
	@Test
	void policyWithCellStatusTest() throws ParseException
	{
		// user object
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse("1992-07-26");
        User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
                false, null, new Role("ROLE_USER"));
 
        // policy object
        Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);
        Policy policy1 = new Policy(2, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);
 
        // nominee object
        Nominee nominee = new Nominee(1, "nominee1", 1234567899);
 
        Collection<UserDependents> l = new ArrayList<UserDependents>();
 
        UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
                0, l, nominee);
        List<UsersPolicy> userPolicies=new ArrayList<>();
        userPolicies.add(usersPolicy);
        List<Policy> policies=new ArrayList<>();
        policies.add(policy);
        policies.add(policy1);
        when(policyRepository.findAll()).thenReturn(policies);
        when(usersPolicyRepository.findByPolicy(policy)).thenReturn(userPolicies);
        assertEquals(2, adminService.policyWithSellStatus().size());
		
		
	}
	
	@Test
	void getPurchasedPolicyTest() {
		Date date = new java.util.Date();
		User user = new User(1, "abc", "abc@gmail.com", "123", "1234567899", Gender.valueOf("Male"), "mg road", date,
				false, null, new Role("ROLE_USER"));
		// policy object
		Policy policy = new Policy(1, "Jeevan Yojna", "Life Policy", Category.valueOf("Life"), 1000, 100000, 1, 45);

		// nominee object
		Nominee nominee = new Nominee(1, "nominee1", 1234567899);

		Collection<UserDependents> l = new ArrayList<UserDependents>();

		UsersPolicy usersPolicy = new UsersPolicy(1, user, policy, Status.valueOf("Approved"), 5000, date, date, false,
				0, l, nominee);
		List<UsersPolicy> list = new ArrayList<UsersPolicy>();
		list.add(usersPolicy);
		when(usersPolicyRepository.findAll()).thenReturn(list);
		assertEquals(1, adminService.getPurchasedPolicy().size());
	}
}
