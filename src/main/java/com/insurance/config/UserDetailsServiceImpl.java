package com.insurance.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.insurance.repository.UserRepository;


@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
    private UserRepository userRepository;
   
    public String currentName()
    {
    var authentication = SecurityContextHolder.getContext().getAuthentication();
    return authentication.getName();
    }
   
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       
        //fetching user from database
       
        var user = userRepository.findByEmail(username);
       
        if(user==null)
        {
            throw new UsernameNotFoundException("Could not found user");
        }
       
        return new CustomUserDetails(user);
       
        
    }
}