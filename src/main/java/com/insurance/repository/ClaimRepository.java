package com.insurance.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.insurance.model.Claim;
import com.insurance.model.UsersPolicy;

@Repository
public interface ClaimRepository extends JpaRepository<Claim, Integer>{
public List<Claim> findByUsersPolicy(UsersPolicy usersPolicy);
}
