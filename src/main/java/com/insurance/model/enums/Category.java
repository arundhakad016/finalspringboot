package com.insurance.model.enums;

public enum Category {
	Life,
	Dental,
	Dental_and_Vision
}
