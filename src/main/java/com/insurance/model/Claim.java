package com.insurance.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "claim")
public class Claim {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "claimed_person")
	private String claimedPerson;

	@Column(name = "claim_date")
	private Date claimDate;

	@Column(name = "claim_amount")
	private int claimAmount;

	@Column(name = "remaining_amount")
	private int remainingAmount;

	@ManyToOne
	@JoinColumn(name = "user_policy_id")
	private UsersPolicy usersPolicy;

	public Claim() {
		super();
	}

	public Claim(int id, String claimedPerson, Date claimDate, int claimAmount, int remainingAmount,
			UsersPolicy usersPolicy) {
		super();
		this.id = id;
		this.claimedPerson = claimedPerson;
		this.claimDate = claimDate;
		this.claimAmount = claimAmount;
		this.remainingAmount = remainingAmount;
		this.usersPolicy = usersPolicy;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClaimedPerson() {
		return claimedPerson;
	}

	public void setClaimedPerson(String claimedPerson) {
		this.claimedPerson = claimedPerson;
	}

	public Date getClaimDate() {
		return claimDate;
	}

	public void setClaimDate(Date claimDate) {
		this.claimDate = claimDate;
	}

	public int getClaimAmount() {
		return claimAmount;
	}

	public void setClaimAmount(int claimAmount) {
		this.claimAmount = claimAmount;
	}

	public int getRemainingAmount() {
		return remainingAmount;
	}

	public void setRemainingAmount(int remainingAmount) {
		this.remainingAmount = remainingAmount;
	}

	public UsersPolicy getUsersPolicy() {
		return usersPolicy;
	}

	public void setUsersPolicy(UsersPolicy usersPolicy) {
		this.usersPolicy = usersPolicy;
	}

}
