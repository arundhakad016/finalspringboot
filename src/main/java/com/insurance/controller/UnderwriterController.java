package com.insurance.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.insurance.model.UsersPolicy;
import com.insurance.service.UnderwriterService;


@RestController
@RequestMapping("/underwriter")
public class UnderwriterController {

	@Autowired 
	private UnderwriterService underwriterService;
	
	@GetMapping("/getUserPoliciesByStatus/{status}")
    public List<UsersPolicy> getUserPoliciesByStatus(@PathVariable("status") String status)
    {
        return underwriterService.getUserPoliciesByStatus(status);
    }
   
    @PutMapping("/updateUsersPolicy")
    public UsersPolicy updateUsersPolicy(@RequestBody UsersPolicy usersPolicy)
    {
        return underwriterService.updateUsersPolicy(usersPolicy);
    }
}
