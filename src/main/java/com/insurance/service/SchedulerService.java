package com.insurance.service;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.insurance.model.UsersPolicy;
import com.insurance.model.enums.Status;
import com.insurance.repository.UsersPolicyRepository;

@Service
public class SchedulerService {

	@Autowired
	private UsersPolicyRepository usersPolicyRepository;

	private AtomicInteger count1 = new AtomicInteger(0);
	private AtomicInteger count2 = new AtomicInteger(0);

	@Scheduled(fixedRate = 5000)
	public void checkingExpiryOfUsersPolicy() {
		List<UsersPolicy> list = usersPolicyRepository.findAll();
		LocalDate currentDate = java.time.LocalDate.now();
		for (var i = 0; i < list.size(); i++) {
			if ((list.get(i).getExpireDate()).toString().substring(0, 10)
					.equals((currentDate).toString().substring(0, 10))) {
				list.get(i).setStatus(Status.valueOf("Completed"));
			}
		}
		this.count1.incrementAndGet();

	}

	@Scheduled(fixedRate = 2 * 24 * 60 * 60 * 1000)
	public void removeRejectedPolicies() {
		List<UsersPolicy> list = usersPolicyRepository.findAll();
		for (var i = 0; i < list.size(); i++) {
			if (list.get(i).getStatus().equals(Status.valueOf("Completed"))) {
				usersPolicyRepository.deleteById(list.get(i).getId());
			}
		}
		this.count2.incrementAndGet();
	}

	public int getInvocationCount1() {
		return this.count1.get();
	}
	
	public int getInvocationCount2() {
		return this.count2.get();
	}
}