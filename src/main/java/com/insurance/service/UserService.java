package com.insurance.service;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.insurance.config.UserDetailsServiceImpl;
import com.insurance.exception.MyApplicationException;
import com.insurance.model.Claim;
import com.insurance.model.Nominee;
import com.insurance.model.Policy;
import com.insurance.model.User;
import com.insurance.model.UserDependents;
import com.insurance.model.UsersPolicy;
import com.insurance.model.enums.Category;
import com.insurance.model.enums.Status;
import com.insurance.repository.ClaimRepository;
import com.insurance.repository.DependentsRepository;
import com.insurance.repository.NomineeRepository;
import com.insurance.repository.PolicyRepository;
import com.insurance.repository.UsersPolicyRepository;
import com.insurance.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PolicyRepository policyRepository;

	@Autowired
	private UsersPolicyRepository usersPolicyRepository;

	@Autowired
	private NomineeRepository nomineeRepository;

	@Autowired
	private DependentsRepository dependentsRepository;

	@Autowired
	private ClaimRepository claimRepository;

	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;

	// Tested
	public List<UserDependents> addDependents(List<UserDependents> dependents) {

		List<UserDependents> l = new ArrayList<>();

		for (var i = 0; i < dependents.size(); i++)
			l.add(dependentsRepository.save(dependents.get(i)));

		return l;
	}

	// Tested
	public List<UserDependents> getAllDependents() {
		return this.dependentsRepository.findByUser(userRepository.findByEmail(userDetailsServiceImpl.currentName()));
	}

	// Tested
	public UserDependents updateDependent(UserDependents dependent) {
		return this.dependentsRepository.save(dependent);
	}

	// Tested
	public User updateUserProfile(User user) {
		return this.userRepository.save(user);
	}

	// Tested
	public List<Policy> getPoliciesByCategory(String cat) {

		LocalDate birthDate = userRepository.findByEmail(userDetailsServiceImpl.currentName()).getDob().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate currentDate = java.time.LocalDate.now();
		int age = Period.between(birthDate, currentDate).getYears();
		List<Policy> policies = policyRepository.findByCategory(Category.valueOf(cat));
		List<Policy> selectedPolicies = new ArrayList<>();
		for (Policy policy : policies) {
			if (policy.getMaxAge() > age)
				selectedPolicies.add(policy);
		}

		return selectedPolicies;
	}

	public int getDependentAge(UserDependents userDependent) {
		LocalDate birthDate = userDependent.getDob().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate currentDate = java.time.LocalDate.now();
		return Period.between(birthDate, currentDate).getYears();
	}

	// Tested
	public List<UserDependents> getDependentsByPolicyMaxAge(int policyMaxAge) {
		User currentUser = userRepository.findByEmail(userDetailsServiceImpl.currentName());
		List<UserDependents> dependents;
		List<UserDependents> selectedDependents = new ArrayList<>();
		dependents = dependentsRepository.findByUser(currentUser);
		for (UserDependents dependent : dependents) {
			if (getDependentAge(dependent) < policyMaxAge)
				selectedDependents.add(dependent);

		}

		return selectedDependents;
	}

	// Tested
	public UsersPolicy purchasePolicy(UsersPolicy usersPolicy) {
		return usersPolicyRepository.save(usersPolicy);
	}

	public List<Claim> getUserClaimsByUserPolicy(int usersPolicyId) throws MyApplicationException {
		Optional<UsersPolicy> optional = usersPolicyRepository.findById(usersPolicyId);
		if(optional.isPresent())
		{
		return claimRepository.findByUsersPolicy(optional.get());
		}
		else
		{
		throw new MyApplicationException("Could not find User's Policy");
		}
	}

	// Tested
	public Claim addClaim(Claim claim) {
		return claimRepository.save(claim);
	}

	// Tested
	public UsersPolicy updateUsersPolicy(UsersPolicy usersPolicy) {
		return this.usersPolicyRepository.save(usersPolicy);
	}

	// Tested
	public List<UsersPolicy> getUserPoliciesByStatus(String status) {
		return usersPolicyRepository.findByStatusAndUser(Status.valueOf(status),
				userRepository.findByEmail(userDetailsServiceImpl.currentName()));
	}

	// Tested
	public Nominee addNominee(Nominee nominee) {
		return this.nomineeRepository.save(nominee);
	}

	// Tested
	public List<UsersPolicy> getAllUserPolicies() {
		return usersPolicyRepository.findByUser(userRepository.findByEmail(userDetailsServiceImpl.currentName()));
	}

}
