package com.insurance.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.insurance.model.UsersPolicy;
import com.insurance.model.enums.Status;
import com.insurance.repository.UsersPolicyRepository;

@Service
public class UnderwriterService {

	@Autowired
	private UsersPolicyRepository usersPolicyRepository;
	

	public UnderwriterService(UsersPolicyRepository usersPolicyRepository) {
		super();
		this.usersPolicyRepository = usersPolicyRepository;
	}


	public List<UsersPolicy> getUserPoliciesByStatus(String status) {
       return usersPolicyRepository.findByStatus(Status.valueOf(status));
       
    }
   
    
    public UsersPolicy updateUsersPolicy(UsersPolicy usersPolicy) {
        return usersPolicyRepository.save(usersPolicy);
    }
	
}
