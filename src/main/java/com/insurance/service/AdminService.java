package com.insurance.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.insurance.exception.MyApplicationException;
import com.insurance.model.Policy;
import com.insurance.model.Role;
import com.insurance.model.User;
import com.insurance.model.UsersPolicy;
import com.insurance.model.dto.SoldPolicyStatus;
import com.insurance.repository.PolicyRepository;
import com.insurance.repository.UsersPolicyRepository;
import com.insurance.repository.UserRepository;

@Service
public class AdminService {

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PolicyRepository policyRepository;

	@Autowired
	private UsersPolicyRepository usersPolicyRepository;

	
	
	//Tested
	public User save(User user) {

		user.setPassword(passwordEncoder.encode(user.getPassword()));

		return userRepository.save(user);
	}

	
	//Tested
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}
	
	//to Check
	public User getUserByUsername(String username)
	{
		return userRepository.findByEmail(username);
	}
	
	
	//Tested
	public List<User> getAllUnderwriters(Role role) {
		return userRepository.findByRole(role);
	}
	
	
	
	//Policy CRUD
	//Tested
	public Policy addPolicy(Policy policy) {
		return policyRepository.save(policy);
	}
	
	//Tested
	public List<Policy> getAllPolicies() {
		return policyRepository.findAll();
	}
	
	//Tested
	public Policy updatePolicy(Policy policy) {
		return policyRepository.save(policy);
	}
	
	//Tested
	public String  deletePolicyById(int id) throws MyApplicationException {
		try
		{
		policyRepository.deleteById(id);
		}
		catch(Exception e)
		{
		throw new MyApplicationException("Unable to delete policy",e);
		}
		return "Policy Deleted SuccessFully with id "+id;
	}
	
	
	//Tested
	public User updateUnderwriter(User user) {
		return userRepository.save(user);
	}
	
	

	
		//Tested
		public String  deleteUnderwriterById(int id) throws MyApplicationException {
			try { 
			userRepository.deleteById(id);
			return "Underwriter Deleted SuccessFully with id "+id;
			}catch(Exception e) {
			throw new MyApplicationException("Failed to delete underwriter having id "+id);
		}
		
}
	

	
	public List<UsersPolicy> getPurchasedPolicy()
	{
		return usersPolicyRepository.findAll();
	}
	
	
	
	//Tested
	public List<SoldPolicyStatus> policyWithSellStatus()
	{
		List<SoldPolicyStatus> soldPolicyStatus=new ArrayList<>();
		List<Policy> policies;
		SoldPolicyStatus soldPolicy;
		policies=policyRepository.findAll();
		for(Policy policy:policies)
		{
			soldPolicy=new SoldPolicyStatus(policy,false);
			//Will return empty list when no result found 
			
			if(!usersPolicyRepository.findByPolicy(policy).isEmpty())
			{
				soldPolicy.setSold(true);
			}
			soldPolicyStatus.add(soldPolicy);
		}
		return soldPolicyStatus;
	}
	
}
